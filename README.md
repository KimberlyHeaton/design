#Fresh tools for web design

Art and design is being part of my job, and I used various tools to build a unique and quaint art for my client. Currently, I’m using a <a href="https://graftene.co.uk/apps-services">mobile app development</a> tool and other software to finish the project in no time.

Are you looking for an effective and easy tool? There are lists of great new tools when creating a responsive design while the consistency and layout is still intact. This list is the tools I used when I needed it most and it includes: Wagtail, Macaw Scarlet, Magic Mirror for Sketch 3, Wire Flow and Wire-frame UI. See sample designs below.



![Screenshot of design](http://netdna.webdesignerdepot.com/uploads/2015/08/wagtail.jpg)

![Screenshot of design](http://netdna.webdesignerdepot.com/uploads/2015/08/wireflow.jpg)